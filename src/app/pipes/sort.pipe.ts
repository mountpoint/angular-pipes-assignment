import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
  pure: false
})
export class SortPipe implements PipeTransform {
  private prop: string;

  transform(value: any[], prop: string, orderBy: 'asc' | 'desc'): any[] {
    this.prop = prop;
    return value.sort(this[orderBy].bind(this));
  }

  private asc(a, b): number {
    if (a[this.prop] > b[this.prop]) {
      return 1;
    } else if (a[this.prop] < b[this.prop]) {
      return -1;
    } else {
      return 0;
    }
  }

  private desc(a, b) {
    if (a[this.prop] < b[this.prop]) {
      return 1;
    } else if (a[this.prop] > b[this.prop]) {
      return -1;
    } else {
      return 0;
    }
  }
}
